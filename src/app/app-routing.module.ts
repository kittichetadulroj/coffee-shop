import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { MyFormComponent } from './components/my-form/my-form.component';
import { DirectivetestComponent } from './components/directivetest/directivetest.component';
import { CoffeeshopComponent } from './components/coffeeshop/coffeeshop.component';
import { GreetComponent } from './components/greet/greet.component';
import { FriendsComponent } from './components/friends/friends.component';


const routes:Routes = [
  {path: 'databind',component:GreetComponent},
  {path: 'form',component:MyFormComponent},
  {path: 'direct',component:DirectivetestComponent},
  {path: 'friend',component:FriendsComponent},
  {path: 'coffee-shop',component:CoffeeshopComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
